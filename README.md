# C_plus_if

## Wykonaj Warunki oraz pętle dla schematów blokowych jak na rysunkach. 

```

```
**## 0.1 )ifFlochart)
w miejsce typu danych **int** , wstaw numer z dziennka .
zamiast zera wstaw dzień swoich urodzin.
użyj operatora <  lub >  który spełni warunek **if**

#### np. 
`int main() {
int a = 10;
if (a>0) {`

*naturalnie ty wykonaj własny przykład* 

![img.png](img.png)

## 02 (ifFlochartB)

w miejsce typu danych **int** , wstaw numer z dziennka .
zamiast zera wstaw dzień swoich urodzin.
* jeśli warunek if się zgodzi , to program ma wyświetlić komunikat:
warunek if jest prawdziwy
* jeśli warunek if nie zgodzi się. Program ma wyświetlić komunikat:
warunek if jest błędny.** 


```       

![img_1.png](img_1.png)

## 03 (switchFlochart)


![img_2.png](img_2.png)

Zatwierdz swoją pracę :
wykonaj: 
**branch** (ImieNazwisko)

wykonaj:
**commit 01** (nzwa zadania);
* commit 02 (nazwa zadania);
* commit 03 (nazwa zadania);
* commit 04 (nazwa zadania); 


04 (Wykonaj samodzielne zadanie, na podstawie schematu blokowego jak niżej)
![img_3.png](img_3.png)

 ### Niżej przedstawiam, obowiązujący w polsce typ schematów blokowych :
![img_4.png](img_4.png)
## Authors and acknowledgment
Bartlomiej Durak
## License
ZST Kolbuszowa
## Project status
rozwojowy